# Burrows–Wheeler transform

## A bioinformatics algorithm used for data compression

This repository contains an implementation of the Burrows–Wheeler transform to compress a character string
