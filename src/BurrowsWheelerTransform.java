/**
 * Compute Burrows-Wheeler encoding, reading from standard input, and writing to standard output
 */

import java.util.Arrays;
import java.util.Scanner;

public class BurrowsWheelerTransform {

    public static String computeSuffixArray(String inputString) {

        /*
         * Encoding
         */

        // Add end symbol
        inputString = inputString + "$";

        // Form all cyclic rotations of the character string
        int stringLength = inputString.length();
        String concatInputString = inputString.concat(inputString);
        String[] suffixArray = new String[stringLength];
        for (int i=0; i < stringLength; i++) {
            suffixArray[i] = concatInputString.substring(i, i + stringLength);
        }

        // Sort the suffix array
        Arrays.sort(suffixArray);

        // Retrieve the last column of the suffix array
        // TODO
        String bwt = "";

        return bwt;

    }

    public static void main(String[] args) {

        Scanner objScanner = new Scanner(System.in);
        System.out.println("Enter a character string:");

        String userInput = objScanner.nextLine();

        System.out.println("Input character string: " + userInput);
        System.out.println("Compressing...");

        String bwtString = computeSuffixArray(userInput);
        
        System.out.println("Burrows-Wheeler transform: " + bwtString);

    }

}